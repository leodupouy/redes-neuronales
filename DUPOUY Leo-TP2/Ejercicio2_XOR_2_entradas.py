#Implemente un perceptron simple que aprenda las funciones logicas XOR de 2 entradas

import numpy
from numpy import *
from random import *
import matplotlib.pyplot as plt

maxLoop = 10000;

#2 entradas
E1 = [1,1,-1,-1]
E2 = [1,-1,1,-1]
Sd = [-1,1,1,-1]
Beta = 1
ECM =[]

def computegGFirst(w, e1, e2):
    h1 = w[0,0] * e1 + w[0,1] * e2 + w[0,2]
    h2 = w[1,0] * e1 + w[1,1] * e2 + w[1,2]
    g1 = tanh(Beta * h1)
    g2 = tanh(Beta * h2)
    return [g1,g2,h1,h2]

def computeGSecond(w,e1,e2):
    h = w[0] * e1 + w[1] * e2 + w[2]
    ret = [tanh(Beta *h),h]
    return ret

def computeAll(w1,wS,e1,e2):
    firstLayer = computegGFirst(w1,e1,e2)
    exit = computeGSecond(wS,firstLayer[0],firstLayer[1])
    ret = [exit,firstLayer]
    return ret

def computeECM(w1,wS) :
    result =0
    for i in range(4):
        temp = computeAll(w1, wS, E1[i], E2[i])[0][0]
        result = result + ((temp - Sd[i])**2)
    return result

def computeAllPrinted(w1,wS) :
    result =0
    for i in range(4):
        result = computeAll(w1,wS, E1[i], E2[i])[0][0]
        print str(E1[i]) + " XOR " + str(E2[i]) + " = " + str(result) +"\nresult theorical = " + str(Sd[i])
    return result


def computeDeltaS(hExit,exitExpected,exitReal):
    gPrime = Beta * (1 - tanh(hExit)**2)
    return gPrime * (exitExpected - exitReal)


W1=numpy.random.randn(2,3)
print W1

Ws = numpy.random.randn(3)
#Ws = Ws[:,0]
print Ws

En=[0,0]

ecm = computeECM(W1,Ws)
#print "ecm = " + str(ecm)
ECM.append(ecm)
i = 0
while (ecm > 0.01 and i < maxLoop) :
#    print i
    i = i+1
    Choosen = randint(0, 3)
    En[0] = E1[Choosen]
    En[1] = E2[Choosen]
    sd = Sd[Choosen]

    s = computeAll(W1,Ws,En[0],En[1])
    exitReal = s[0][0]
    hS = s[0][1]
    g1 = s[1][0]
    g2 = s[1][1]
    h1 = s[1][2]
    h2 = s[1][3]
    deltaW = computeDeltaS(hS, sd, exitReal)
#    print "deltaW = " + str(deltaW)

    deltaW1 = Beta * (1 - (tanh(h1))**2) * Ws[0]*deltaW
    deltaW2 = Beta * (1 - (tanh(h2))**2) * Ws[1]*deltaW
    ecm = computeECM(W1,Ws)
#    print "ecm =" + str(ecm) + "\n"
    Ws[0] = Ws[0] + deltaW*g1
    Ws[1] = Ws[1] + deltaW*g2
    Ws[2] = Ws[2] + deltaW

    W1[0, 0] = W1[0, 0] + En[0] * deltaW1
    W1[1, 0] = W1[1, 0] + En[0] * deltaW2
    W1[0, 1] = W1[0, 1] + En[1] * deltaW1
    W1[1, 1] = W1[1, 1] + En[1] * deltaW2
    W1[0, 2] = W1[0, 2] + deltaW1
    W1[1, 2] = W1[1, 2] + deltaW2

    ECM.append(ecm)

#    print deltaW
#    print ecm

computeAllPrinted(W1,Ws)
plt.title('evolution of ECM')
plt.plot(ECM)
plt.ylabel('ECM')
plt.show()

