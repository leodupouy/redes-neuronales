#Implemente un perceptron simple que aprenda las funciones logicas OR de 4
#entradas
import numpy
from numpy import *
from random import *
import matplotlib.pyplot as plt

#2 entradas

E1 = [1, 1, 1, 1,-1,-1,-1,-1, 1, 1, 1,-1,-1,-1, 1,-1]
E2 = [1, 1, 1,-1, 1, 1, 1,-1,-1,-1, 1,-1,-1, 1,-1,-1]
E3 = [1, 1,-1, 1, 1, 1,-1, 1,-1, 1,-1,-1, 1,-1,-1,-1]
E4 = [1,-1, 1, 1, 1,-1, 1, 1, 1,-1,-1, 1,-1,-1,-1,-1]

Sd = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,-1]
ECM = []
def computeECM(w) :
    result = 0;
    for i in range(16):
        result = result + (computeAnswer(w, [E1[i], E2[i],E3[i], E4[i]]) - Sd[i])**2;
    return result;

def computeDeltaW(sd,s,eta) :
    return eta*(sd-s);

def computeAnswer(w, e):
    if(w[0] + w[1] * e[0] + w[2] * e[1] + w[3] * e[2] + w[4] * e[3]) >= 0 :
        return 1;
    else :
        return -1;

En=[0,0,0,0]

W=numpy.random.randn(5,1);
print W
ecm = computeECM(W);
print "ecm = " + str(ecm)
ECM.append(ecm)
while (ecm != 0) :
    En[0] = randint(0,1)*2 -1;
    En[1] = randint(0, 1) * 2 - 1;
    En[2] = randint(0, 1) * 2 - 1;
    En[3] = randint(0, 1) * 2 - 1;
    sd = ((En[0]+1)/2) and ((En[1]+1)/2) and ((En[2]+1)/2) and ((En[3]+1)/2);
    s = computeAnswer(W,En);
    print str(En[0]) + " AND " + str(En[1])  + " AND " + str(En[2]) + " AND " + str(En[3])+ " = " + str(s)
    deltaW = computeDeltaW(sd, s, 1);
    print "deltaW = " + str(deltaW)
    W[0] = W[0] + deltaW;
    for i in range(4):
        W[i+1] = W[i+1] + deltaW * En[i];
    ecm = computeECM(W);
    print "ecm =" + str(ecm) + "\n"
    ECM.append(ecm)

plt.title('evolution of ECM')
plt.plot(ECM)
plt.ylabel('ECM')
plt.show()

print "Answer with 1 and 1 and 1 and 1 = " + str(computeAnswer(W,[1,1,1,1]))

print "Answer with 1 and -1 and -1 and -1 = " + str(computeAnswer(W,[1,-1,-1,-1]))


print "Answer with -1 and 1 and 1 and 1= " + str(computeAnswer(W,[-1,1,1,1]))

print "Answer with -1 and -1 and -1 and -1= " + str(computeAnswer(W,[-1,-1,-1,-1]))

