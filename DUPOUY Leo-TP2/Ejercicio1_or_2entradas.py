#Implemente un perceptron simple que aprenda las funciones logicas OR de 2
#entradas
import numpy
from numpy import *
from random import *
import matplotlib.pyplot as plt

#2 entradas
E1 = [1,1,-1,-1]
E2 = [1,-1,1,-1]
Sd = [1,1,1,-1]
ECM = []

def computeECM(w) :
    result =0;
    for i in range(4):
        result = result + (computeAnswer(w, E1[i], E2[i]) - Sd[i])**2;
    return result;

def computeDeltaW(sd,s,eta) :
    return eta*(sd-s);

def computeAnswer(w, e1, e2):
    if(w[0] + w[1] * e1 + w[2] * e2) >= 0 :
        return 1;
    else :
        return -1;



W=numpy.random.randn(3,1);
print W
ecm = computeECM(W);
ECM.append(ecm)
print "ecm = " + str(ecm)
while (ecm != 0) :
    e1 = randint(0,1)*2 -1;
    e2 = randint(0,1)*2 -1;
    sd = ((e1+1)/2) and ((e2+1)/2);
    s = computeAnswer(W,e1,e2);
    print str(e1) + " AND " + str(e2) + " = " + str(s)
    deltaW = computeDeltaW(sd, s, 1);
    print "deltaW = " + str(deltaW)
    W[0] = W[0] + deltaW;
    W[1] = W[1] + deltaW * e1;
    W[2] = W[2] + deltaW * e2;
    ecm = computeECM(W);
    print "ecm =" + str(ecm) + "\n"
    ECM.append(ecm)

plt.title('evolution of ECM')
plt.plot(ECM)
plt.ylabel('ECM')
plt.show()
print "Answer with 1 and 1 = " + str(computeAnswer(W,1,1))

print "Answer with 1 and -1 = " + str(computeAnswer(W,1,-1))


print "Answer with -1 and 1 = " + str(computeAnswer(W,-1,1))

print "Answer with -1 and -1 = " + str(computeAnswer(W,-1,-1))

