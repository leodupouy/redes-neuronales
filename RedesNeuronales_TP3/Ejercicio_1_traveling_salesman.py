#
import time
from numpy import *
import numpy
from random import *
import matplotlib.pyplot as plt


#W = sort(W)
eta = 0.5
sigma = 0.5
def neighbourValue(W,originIndice,sigma) :
    neighbourValues = []
    for i in range(nbNeurons) :
        neighbourValues.append(exp(-1*(distance([cos(2*pi*i/nbNeurons),sin(2*pi*i/nbNeurons)],[cos(2*pi*originIndice/nbNeurons),sin(2*pi*originIndice/nbNeurons)])**2)/(2*(sigma**2))))
        #print "dist : "+str((((originIndice*1.0)-(i*1.0))/(nbNeurons*1.0))**2)+"  div : "+str(2*(sigma**2)) +"... nbNeur : "+str(nbNeurons)+"  sigma : "+str(sigma)
    return neighbourValues

#euclidian
def distance(a,b) :
    return sqrt((a[0]-b[0])**2+(a[1]-b[1])**2)

def findNeuron(W,entryValue) :
    distance_min = 10
    result = 0
    for i in range(len(W)) :
        actual_distance = distance(W[i],entryValue)
        if (actual_distance < distance_min) :
            distance_min = actual_distance
            result = i
    return result

def computeDeltaW(W,entryValue,sigma,eta) :
    originIndice = findNeuron(W,entryValue)
    neighbourValues = neighbourValue(W, originIndice,sigma)
    differences = -1 * (W - entryValue)
    differencesX = []
    differencesY = []
    for diff in differences :
        differencesX.append(diff[0])
        differencesY.append(diff[1])
    neighValues = []
    for value in neighbourValues :
        neighValues.append([value,value])
    dW = eta * numpy.asarray(neighValues) * numpy.asarray(differences)
    #print neighValues[originIndice]
    #print dW[originIndice]
    return dW

def printGraphPath(W,entries,solution) :
    entryX = []
    entryY = []
    wX = []
    wY = []
    for entry in entries :
        entryX.append(entry[0])
        entryY.append(entry[1])
    for w in W :
        wX.append(w[0])
        wY.append(w[1])
    toPrintX = numpy.array(wX)[numpy.array(solution)]
    toPrintY = numpy.array(wY)[numpy.array(solution)]
    plt.plot(toPrintX, toPrintY, marker='o', c='lime', markersize=5)
    plt.plot(entryX,entryY, linestyle='none', marker='X', c='red', markersize=10)
    plt.plot([toPrintX[0],toPrintX[-1]],[toPrintY[0],toPrintY[-1]], markersize=0)
    plt.plot(W[solution[0]][0],W[solution[0]][1], marker='o', c='blue', markersize=10)
    plt.plot(W[solution[1]][0],W[solution[1]][1], marker='o', c='yellow', markersize=7)
    plt.show()
    return

def printGraph(W,entries, sigma, eta) :
    entryX = []
    entryY = []
    wX = []
    wY = []
    for entry in entries :
        entryX.append(entry[0])
        entryY.append(entry[1])
    for w in W :
        wX.append(w[0])
        wY.append(w[1])
    plt.plot(wX, wY, marker='o', c='lime', markersize=5)
    plt.plot(entryX,entryY, linestyle='none', marker='X', c='red', markersize=5)
    plt.plot([wX[0],wX[-1]], [wY[0],wY[-1]], marker='o', c='blue', markersize=0)

    plt.title("sigma : "+str(sigma)+"  eta : "+str(eta))
    plt.show()
    return

def computeECM(W) :
    ecm = 0
    #for value in entries :

def learning(W,entries,maxLoop,nbCities,nbNeurons, sigma,eta):

    i = 0.0
    lastpercent = 1
    start_time = time.time()
    intermediate_time = start_time
    while i < maxLoop :
        i = i + 1
        #sigma = sigma - (maxSigma * (1 / (maxLoop + 1.0)))
        #if(sigma < 0.001):
        #    sigma = 0.001
        if ((i / maxLoop) > (lastpercent / 100.0)):
            elapsed_time = time.time() - intermediate_time

            intermediate_time = time.time()
            print "estimated time left : " + str(elapsed_time * (100 - lastpercent)).split(".")[0] + "seconds"
            print str(lastpercent) + "%"
            lastpercent = lastpercent + 1
        Choosen = randint(0, nbCities-1)
        value= entries[Choosen]
        deltaW = computeDeltaW(W, value,sigma,eta)
        W = W + deltaW
        if (i % 10 == 0):
          printGraph(W, entries, sigma, eta)
        sigma = sigma * 0.995
        eta = eta
    return W
    #eta = eta * (1/i)

def computePath(W,cityA):
    solution = []
    nW = copy(W)
    first = findNeuron(nW,cityA)
    current = copy(nW[first])
    #W = numpy.delete(W, first, axis=0)
    nW[first]=[infini,infini]
    solution.append(first)
    while(size(solution) < nbNeurons):
        next = findNeuron(nW,current)
        current = copy(nW[next])
        solution.append(next)
        nW[next] = [infini,infini]
        #W = numpy.delete(W, next, axis=0)
    return solution

def computeIndiceCorrespondance(W,entries) :
    citiesCorrespondance = []
    for entry in entries :
        citiesCorrespondance.append(findNeuron(W,entry))
    return citiesCorrespondance


def constructCircle(nbEntries) :
    result = []
    for i in range(nbEntries) :
        result.append([cos(2*pi*i/nbEntries),sin(2*pi*i/nbEntries)])
    result = numpy.array(result) + 1
    result = result/2
    return result

eta = 0.5
sigma = 0.5
nbCities = 100
nbNeurons = 250
infini = 10000
entries = (numpy.random.rand(nbCities,2) )
Wp = constructCircle(nbNeurons)

maxLoop = 1000
printGraph(Wp, entries, sigma, eta)

newW=learning(copy(Wp),copy(entries),maxLoop,nbCities,nbNeurons,sigma,eta)
#print entries

#solution = computePath(copy(newW),entries[randint(0,nbCities-1)])
printGraph(newW,entries,sigma, eta)
#print newW
#printGraph(W,entries)
