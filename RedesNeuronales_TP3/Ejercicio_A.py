#
import time
from numpy import *
import numpy
import random
import matplotlib.pyplot as plt

def distribution(xS,alpha):
    distrib = []
    coef = 1
    #coef = alpha+1.0
    for i in xS:

        distrib.append(coef*(i**alpha))
    return [distrib,xS]

def distributionW(xS,alpha,beta):
    distrib = []
    coef = 1
    # coef = alpha+1.0
    for i in xS:
        distrib.append(coef*(i**beta))
    return [distrib,xS]

def distributionRand(xMax,alpha):
    distrib = []
    xS = numpy.random.rand(xMax)
    xS= numpy.sort(xS)
    coef = 1
    # coef = alpha+1.0
    for x in xS:
        distrib.append(coef*(x**alpha))
    return [distrib,xS]

def printGraph(W,entries,xW,xS, sigma, eta) :
    nW = copy(W)
    if(W[0] > W[-1]):
        for i in range(len(W)):
            nW[i] = W[len(W)-i-1]
    #nW = numpy.sort(numpy.array(nW))
    plt.plot(xW, nW, linestyle='none', marker='o', c='lime', markersize=5)
    plt.plot(xS, entries, linestyle='none', marker='X', c='red', markersize=2)
    plt.title("sigma : "+str(sigma)+"  eta : "+str(eta))
    plt.show()
    return
def neighbourValue(originIndice) :
    neighbourValues = []
    for i in range(nbNeurons) :
        neighbourValues.append(exp(-1*(((originIndice-i)/(nbNeurons*1.0))**2)/(2*(sigma**2))))
    #print neighbourValues
    return neighbourValues

def distance(a,b) :
    return abs(a-b)

def findNeuron(W,entryValue) :
    distance_min = 10
    result = 0
    for i in range(len(W)):
        actual_distance = distance(W[i], entryValue)
        #print actual_distance
        if (actual_distance < distance_min):
            distance_min = actual_distance
            result = i
    #print result
    return result

def computeDeltaW(W,entryValue) :
    originIndice = findNeuron(W,entryValue)
    neighbourValues = neighbourValue(originIndice)
    differences = -1 * (W - entryValue)
    return eta * numpy.array(neighbourValues) * numpy.array(differences)

def printDifExpectedBeta(W,entries,xS):

    return

eta = 0.7
sigma = 0.7
maxSigma = sigma
alpha = 0.6
beta = 1.0/(1+((2.0*alpha)/3))
xMax = 20
nbNeurons = xMax
W = numpy.random.rand(nbNeurons)
xS = numpy.array(range(xMax))/(xMax*1.0)
[entries,xS] = distribution(xS,alpha)
Wperfect = distributionW(xS,alpha,beta)[0]

maxLoop = nbNeurons * nbNeurons * nbNeurons * 2
i = 1.0
lastpercent = 1
start_time = time.time()
intermediate_time = start_time
nEntries = copy(entries)
random.shuffle(nEntries)
while i <= maxLoop :
    i = i + 1
    eta = eta * 0.99999
    if(i%nbNeurons==0):
        random.shuffle(nEntries)
    #sigma=sigma-(maxSigma*(1/(maxLoop+1.0)))
    if (sigma >= 0.1):
        sigma = sigma * 0.99
    else:
        sigma = sigma * 0.9999
    #if(sigma < 0.0001):
    #    sigma = 0.0001
    if ((i / maxLoop) > (lastpercent / 100.0)):
        elapsed_time = time.time() - intermediate_time

        intermediate_time = time.time()
        print "estimated time left : " + str(elapsed_time * (100 - lastpercent)).split(".")[0] + "seconds"
        print str(lastpercent) + "%"
        lastpercent = lastpercent + 1
    Choosen = (int(i-1)%xMax)
    value= nEntries[Choosen]
    deltaW = computeDeltaW(W, value)
    W = W + deltaW

    if(i%2000 == 0):
        printGraph(W, entries,xS,xS, sigma, eta)

printGraph(W,entries,xS,xS,sigma,eta)
