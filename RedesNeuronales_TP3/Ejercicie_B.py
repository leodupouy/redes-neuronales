#
import time
from numpy import *
import numpy
from random import *
import matplotlib.pyplot as plt

eta = 0.1
sigma = 0.1

nbNeurons = 100
nbEntries = nbNeurons

W = (numpy.random.rand(nbNeurons,2) * 2) - 1
#W = sort(W)

def constructCircle() :
    result = []
    for i in range(nbNeurons) :
        result.append([cos(2*pi*i/nbEntries),sin(2*pi*i/nbEntries)])
    return result

entries = constructCircle()

def neighbourValue(W,originIndice) :
    neighbourValues = []
    for val in W :
        neighbourValues.append(exp(-1*(distance(W[originIndice],val)**2))/2*(sigma**2))
    return neighbourValues

#euclidian
def distance(a,b) :
    return sqrt((a[0]-b[0])**2+(a[1]-b[1])**2)

def findNeuron(W,entryValue) :
    min = 0
    max = nbNeurons -1
    distance_min = 10
    result = 0;
    for i in range(len(W)) :
        actual_distance = distance(W[i],entryValue)
        if (actual_distance < distance_min) :
            distance_min = actual_distance
            result = i
        return result

def computeDeltaW(W,entryValue) :
    originIndice = findNeuron(W,entryValue)
    neighbourValues = neighbourValue(W, originIndice)
    differences = -1 * (W - entryValue)
    differencesX = []
    differencesY = []
    for diff in differences :
        differencesX.append(diff[0])
        differencesY.append(diff[1])
    neighValues = []
    for value in neighbourValues :
        neighValues.append([value,value])
    return eta * numpy.asarray(neighValues) * numpy.asarray(differences)


def printGraph(W,entries) :
    entryX = []
    entryY = []
    wX = []
    wY = []
    for entry in entries :
        entryX.append(entry[0])
        entryY.append(entry[1])
    for w in W :
        wX.append(w[0])
        wY.append(w[1])
    plt.plot(wX, wY, linestyle='none', marker='o', c='lime', markersize=10)
    plt.plot(entryX,entryY, linestyle='none', marker='X', c='red', markersize=10)
    plt.show()
    return
entries = constructCircle()

maxLoop = nbNeurons
i = 0
while i < maxLoop :
    i = i + 1
    for value in entries :
        deltaW = computeDeltaW(W, value)
        W = W + deltaW

    #eta = eta * (1/i)


printGraph(W,entries)
