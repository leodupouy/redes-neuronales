#
import time
from numpy import *
import numpy
import random

import matplotlib.pyplot as plt

import matplotlib.tri as tri


def constructSigmas(minSig,maxSig):
    result = []
    for i in range(nbNeurons):
        result.append((maxSig-minSig) * ((1.0)/(i+1))+minSig)
    return result

def constructCircle() :
    result = []
    for i in range(nbEntries) :
        result.append([cos(2*pi*i/nbEntries),sin(2*pi*i/nbEntries)])
    return result


def neighbourValue(W,originIndice) :
    neighbourValues = numpy.random.rand(nbNeurons,nbNeurons,2)
    for i in range(nbNeurons) :
        for j in range(nbNeurons) :
            dist = distance(numpy.asarray(originIndice)+(1.0),numpy.asarray([i,j])+(1.0))**2
            #print dist
            value = exp(-1*(dist)/(2*(sigma**2)))
            neighbourValues[i][j]=[value,value]
    return neighbourValues

#euclidian
def distance(a,b) :
    return ((a[0]-b[0])**2+(a[1]-b[1])**2)

def findNeuron(W,entryValue) :
    distance_min = 10
    result = 0
    for i in range(nbNeurons) :
        for j in range(nbNeurons) :
            actual_distance = distance(W[i][j],entryValue)
            if (actual_distance < distance_min) :
                distance_min = actual_distance
                result = [i,j]
    return result

def computeDeltaW(W,entryValue) :
    originIndice = findNeuron(W,entryValue)
    neighbourValues = neighbourValue(W, originIndice)
    differences = -1 * (W - entryValue)
    #print "entry val : "+str(entryValue)+" W[4][4] = "+str(W[4][4])+" dif : "+str(differences[4,4])
    #print "neig : "+str(neighbourValues[0,0]) + "origin indice : "+str(originIndice)
    return eta * numpy.asarray(neighbourValues) * numpy.asarray(differences)


def printGraph(W,entries,sigma,eta,withPath) :
    entryX = []
    entryY = []
    wX = []
    wY = []
    for entry in entries :
        entryX.append(entry[0])
        entryY.append(entry[1])
    for w in W :
        wX.append(w[0])
        wY.append(w[1])
    plt.plot(W[:, :, 0], W[:, :, 1], marker='o', c='lime', markersize=5)
    plt.plot(numpy.transpose(W[:, :, 0]), numpy.transpose(W[:, :, 1]), marker='o', c='blue', markersize=0)
    plt.plot(entryX,entryY, linestyle='none', marker='X', c='red', markersize=5)
    plt.title("Sigma : "+str(sigma)+"    eta: "+str(eta))
    plot = plt.show()
    return plot

def constructNeigh(W):
    vecinos = numpy.eye(nbNeurons,nbNeurons)
    for i in range(nbNeurons):
        nbRelation = -1
        nW = copy(W)
        for j in range(nbNeurons):
            nbRelation = nbRelation+vecinos[i,j]
            if(vecinos[i,j]==1 and i!=j):
                nW[j] = [1000,1000]
        k = nbRelation
        while (k<4):
            neigh = findNeuron(nW,W[i])
            nW[neigh] = [1000,1000]
            nrelations = -1
            for j in range(nbNeurons):
                nrelations = nrelations+vecinos[neigh,j]
            if (nrelations < 4):
                vecinos[i,neigh]=1
                vecinos[neigh,i]=1
                k=k+1
    #print vecinos
    return vecinos

def computeECM(W) :
    ecm = 0
    #for value in entries :


eta = 0.3
sigma = 100
maxSigma = copy(sigma)

nbNeurons = 10
nbEntries = 100
maxLoop = nbEntries *2
W = (numpy.random.rand(nbNeurons,nbNeurons,2) * 2) - 1
entries = constructCircle()
print W[:,:,0]
i = 0.0
lastpercent = 1
start_time = time.time()
intermediate_time = start_time
mitad = maxLoop/2.0
nEntries = range(nbEntries)
random.shuffle(nEntries)
print entries
print nEntries
while i < maxLoop :
    i = i + 1
    eta = eta * 0.995
    if (i < (mitad)):
        deltaSigma = maxSigma*0.995/(mitad)
        sigma = sigma - deltaSigma
    else:
        deltaSigma = maxSigma * 0.04 /(mitad)
        sigma = sigma - deltaSigma
    if ((i / maxLoop) > (lastpercent / 100.0)):
        elapsed_time = time.time() - intermediate_time

        intermediate_time = time.time()
        print "estimated time left : " + str(elapsed_time * (100 - lastpercent)).split(".")[0] + "seconds"
        print str(lastpercent) + "%"
        lastpercent = lastpercent + 1
    Choosen = nEntries[int(i-1)%nbEntries]
    value= entries[Choosen]
    deltaW = computeDeltaW(W, value)
    W = W + deltaW

    if(i%3 == 0):
        plt.close()
        printGraph(W, entries, sigma, eta,False)

    #eta = eta * (1/i)

printGraph(W,entries, sigma, eta,True)
