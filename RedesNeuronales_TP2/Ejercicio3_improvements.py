#Implemente una red con aprendizaje Backpropagation que aprenda la siguiente funcion
#f(x,y,z) = (sin x + cos y + z)/3
#donde: x e y estan en el intervalo [0;2pi]
#z esta en el intervalo[-1;1]

#10 valores para cada variable

import numpy
import time
from numpy import *
from random import *
import matplotlib.pyplot as plt

noEcm = True #To skip the computation of the ecm,
# this option on true will make the network really fast
# but it will be impossible to see the evolution of the ECM
rapidMod = True #This option is made to make the network learning faster, on true the ECM will be computed only each 3 loop
# and for half the values we know
maxLoop = 2000000 #This option is giving the number of loops of learning the network will do before stopping
#3 entradas
X = arange(0, 2 * pi+0.001, 0.2 * pi)
print "X = " + str(X)
Y = arange(0, 2 * pi+0.001, 0.2 * pi)
print "Y = " + str(Y)
Z = arange(-1, 1+0.001, 0.2)
print "Z = " + str(Z)

sizeEntries = 11
Sd = []
for x in X:
    Sd1=[]
    for y in Y:
        Sd2= []
        for z in Z:
            Sd2.append((sin(x) + cos(y) + z)/3)
        Sd1.append(Sd2)
    Sd.append(Sd1)
Eta = 0.5
Beta = 0.6
ECM =[]
def computegGFirst(w, e1, e2, e3):
    h1 = w[0, 0] * e1 + w[0, 1] * e2 + w[0, 2] * e3 + w[0, 3] + w[0, 4] + w[0,5]
    h2 = w[1, 0] * e1 + w[1, 1] * e2 + w[1, 2] * e3 + w[1, 3] + w[1, 4] + w[1,5]
    h3 = w[2, 0] * e1 + w[2, 1] * e2 + w[2, 2] * e3 + w[2, 3] + w[2, 4] + w[2,5]
    h4 = w[3, 0] * e1 + w[3, 1] * e2 + w[3, 2] * e3 + w[3, 3] + w[3, 4] + w[3,5]
    h5 = w[4, 0] * e1 + w[4, 1] * e2 + w[4, 2] * e3 + w[4, 3] + w[4, 4] + w[4,5]
    h6 = w[5, 0] * e1 + w[5, 1] * e2 + w[5, 2] * e3 + w[5, 3] + w[5, 4] + w[5,5]
    h7 = w[6, 0] * e1 + w[6, 1] * e2 + w[6, 2] * e3 + w[6, 3] + w[6, 4] + w[6,5]

    g1 = tanh(Beta * h1)
    g2 = tanh(Beta * h2)
    g3 = tanh(Beta * h3)
    g4 = tanh(Beta * h4)
    g5 = tanh(Beta * h5)
    g6 = tanh(Beta * h6)
    g7 = tanh(Beta * h7)

    return [[g1,g2,g3,g4,g5,g6,g7],[h1,h2,h3,h4,h5,h6,h7]]

def computeGSecond(w,e1,e2,e3,e4,e5,e6,e7):
    h = w[0] * e1 + w[1] * e2 + w[2] * e3 + w[3] * e4 + w[5] * e6 + w[4] * e5 + w[6] * e7 + w[7] + w[8]
    ret = [tanh(Beta *h),h]
    return ret

def computeAll(w1,wS,e1,e2,e3):
    firstLayer = computegGFirst(w1,e1,e2,e3)
    exit = computeGSecond(wS,firstLayer[0][0],firstLayer[0][1],firstLayer[0][2],firstLayer[0][3],firstLayer[0][4],firstLayer[0][5],firstLayer[0][6])
    ret = [exit,firstLayer]
    return ret

def computeECM(w1,wS) :
    result =0
    if not rapidMod :
        for i in range(sizeEntries):
            for j in range(sizeEntries) :
                for k in range(sizeEntries) :
                    temp = computeAll(w1, wS, X[i], Y[j], Z[k])[0][0]
                    result = result + ((temp - Sd[i][j][k])**2)
    else :
        for i in range(sizeEntries/2):
            for j in range(sizeEntries/2) :
                for k in range(sizeEntries/2) :
                    temp = computeAll(w1, wS, X[i*2], Y[j*2], Z[k*2])[0][0]
                    result = result + ((temp - Sd[i*2][j*2][k*2])**2)
    return result

def computeAllPrinted(w1,wS) :
    results = []
    for i in range(sizeEntries):
        for j in range(sizeEntries) :
            for k in range(sizeEntries) :
                result = computeAll(w1,wS, X[i], Y[j], Z[k])[0][0]
                results.append(result)
                #print "sin(" + str(X[i]) + ") + cos(" + str(Y[i]) + ") + " + str(Z[i]) + " = " + str(result) +"\nresult theorical = " + str(Sd[i][j][k])
    return results


def computeDeltaS(hExit,exitExpected,exitReal):
    gPrime = Eta * Beta * (1 - tanh(hExit)**2)
    return gPrime * (exitExpected - exitReal)

def printECM(ecms) :
    plt.title('evolution of ECM')
    plt.plot(ecms)
    plt.ylabel('ECM')
    plt.show()

def printGraphAll(expectedResults, results):
    plt.title(
        'Comparison of the expected results and the real results\nNeurons : 7 Beta : ' + str(Beta) + "Eta : " + str(
            Eta) + "\nLoops : " + str(loopsDone))
    plt.plot(expectedResults, 'r', results, 'b')
    plt.show()

def printGraphXVariation(yValue, zValue, w1, wS):
    xs = arange(0, 2 * pi + 0.001, 0.1 * pi)
    networkResults = []
    perfectResults = []
    for xValue in xs :
        networkResults.append(computeAll(w1,wS,xValue,yValue,zValue)[0][0]*3)
        perfectResults.append(sin(xValue) + cos(yValue) + zValue)
    plt.title(
        'Comparison of the expected results and the real results for the variation of x\n y value : '+str(yValue)+' z value : '+str(zValue)+'\nNeurons : 7 Beta : ' + str(Beta) + "Eta : " + str(
            Eta) + "\nLoops : " + str(loopsDone))
    plt.plot(networkResults, 'b', perfectResults, 'r')
    plt.show()


def printGraphYVariation(xValue, zValue, w1, wS):
    ys = arange(0, 2 * pi + 0.001, 0.1 * pi)
    networkResults = []
    perfectResults = []
    for yValue in ys :
        networkResults.append(computeAll(w1,wS,xValue,yValue,zValue)[0][0]*3)
        perfectResults.append(sin(xValue) + cos(yValue) + zValue)
    plt.title(
        'Comparison of the expected results and the real results for the variation of y\n x value : '+str(xValue)+' z value : '+str(zValue)+'\nNeurons : 7 Beta : ' + str(Beta) + "Eta : " + str(
            Eta) + "\nLoops : " + str(loopsDone))
    plt.plot(networkResults, 'b', perfectResults, 'r')
    plt.show()

def printGraphZVariation(xValue, yValue, w1, wS):
    zs = arange(-1, 1 + 0.001, 0.1)
    networkResults = []
    perfectResults = []
    for zValue in zs:
        networkResults.append(computeAll(w1, wS, xValue, yValue, zValue)[0][0] * 3)
        perfectResults.append(sin(xValue) + cos(yValue) + zValue)
    plt.title(
        'Comparison of the expected results and the real results for the variation of z\n x value : ' + str(
            xValue) + ' y value : ' + str(yValue) + '\nNeurons : 7 Beta : ' + str(Beta) + "Eta : " + str(
            Eta) + "\nLoops : " + str(loopsDone))
    plt.plot(networkResults, 'b', perfectResults, 'r')
    plt.show()

W1 = numpy.random.randn(7,6)
print W1

Ws = numpy.random.randn(9)
Ws = Ws
#Ws = Ws[:,0]
print Ws

ecm = computeECM(W1,Ws)
#print "ecm = " + str(ecm)
ECM.append(ecm)
i = 0.0
lastpercent = 1
start_time = time.time()
intermediate_time = start_time
while (ecm > 0.05 and i < maxLoop) :
#    print i
    i = i+1
    if ((i/maxLoop) > (lastpercent/100.0)) :
        elapsed_time = time.time() - intermediate_time

        intermediate_time = time.time()
        print "estimated time left : " + str(elapsed_time * (100 - lastpercent)).split(".")[0] + "seconds"
        print str(lastpercent) + "%"
        lastpercent = lastpercent + 1
    choosenX = randint(0, sizeEntries-1)
    choosenY = randint(0, sizeEntries-1)
    choosenZ = randint(0, sizeEntries-1)

    x = X[choosenX]
    y = Y[choosenY]
    z = Z[choosenZ]

    sd = Sd[choosenX][choosenY][choosenZ]

    s = computeAll(W1,Ws,x,y,z)
    exitReal = s[0][0]
    hS = s[0][1]
    g1 = s[1][0][0]
    g2 = s[1][0][1]
    g3 = s[1][0][2]
    g4 = s[1][0][3]
    g5 = s[1][0][4]
    g6 = s[1][0][5]
    g7 = s[1][0][6]

    h1 = s[1][1][0]
    h2 = s[1][1][1]
    h3 = s[1][1][2]
    h4 = s[1][1][3]
    h5 = s[1][1][4]
    h6 = s[1][1][5]
    h7 = s[1][1][6]

    deltaW = computeDeltaS(hS, sd, exitReal)
#    print "deltaW = " + str(deltaW)

    deltaW1 = Eta * Beta * (1 - (tanh(h1)) ** 2) * Ws[0] * deltaW
    deltaW2 = Eta * Beta * (1 - (tanh(h2)) ** 2) * Ws[1] * deltaW
    deltaW3 = Eta * Beta * (1 - (tanh(h3)) ** 2) * Ws[2] * deltaW
    deltaW4 = Eta * Beta * (1 - (tanh(h4)) ** 2) * Ws[3] * deltaW
    deltaW5 = Eta * Beta * (1 - (tanh(h5)) ** 2) * Ws[4] * deltaW
    deltaW6 = Eta * Beta * (1 - (tanh(h6)) ** 2) * Ws[5] * deltaW
    deltaW7 = Eta * Beta * (1 - (tanh(h7)) ** 2) * Ws[6] * deltaW

    Ws[0] = Ws[0] + deltaW * g1
    Ws[1] = Ws[1] + deltaW * g2
    Ws[2] = Ws[2] + deltaW * g3
    Ws[3] = Ws[3] + deltaW * g4
    Ws[4] = Ws[4] + deltaW * g5
    Ws[5] = Ws[5] + deltaW * g6
    Ws[6] = Ws[6] + deltaW * g7
    Ws[7] = Ws[7] + deltaW
    Ws[8] = Ws[8] + deltaW

    W1[0, 0] = W1[0, 0] + x * deltaW1
    W1[0, 1] = W1[0, 1] + y * deltaW1
    W1[0, 2] = W1[0, 2] + z * deltaW1
    W1[0, 3] = W1[0, 3] + deltaW1
    W1[0, 4] = W1[0, 4] + deltaW1
    W1[0, 5] = W1[0, 5] + deltaW1

    W1[1, 0] = W1[1, 0] + x * deltaW2
    W1[1, 1] = W1[1, 1] + y * deltaW2
    W1[1, 2] = W1[1, 2] + z * deltaW2
    W1[1, 3] = W1[1, 3] + deltaW2
    W1[1, 4] = W1[1, 4] + deltaW2
    W1[1, 5] = W1[1, 5] + deltaW2

    W1[2, 0] = W1[2, 0] + x * deltaW3
    W1[2, 1] = W1[2, 1] + y * deltaW3
    W1[2, 2] = W1[2, 2] + z * deltaW3
    W1[2, 3] = W1[2, 3] + deltaW3
    W1[2, 4] = W1[2, 4] + deltaW3
    W1[2, 5] = W1[2, 5] + deltaW3

    W1[3, 0] = W1[3, 0] + x * deltaW4
    W1[3, 1] = W1[3, 1] + y * deltaW4
    W1[3, 2] = W1[3, 2] + z * deltaW4
    W1[3, 3] = W1[3, 3] + deltaW4
    W1[3, 4] = W1[3, 4] + deltaW4
    W1[3, 5] = W1[3, 5] + deltaW4

    W1[4, 0] = W1[4, 0] + x * deltaW5
    W1[4, 1] = W1[4, 1] + y * deltaW5
    W1[4, 2] = W1[4, 2] + z * deltaW5
    W1[4, 3] = W1[4, 3] + deltaW5
    W1[4, 4] = W1[4, 4] + deltaW5
    W1[4, 5] = W1[4, 5] + deltaW5

    W1[5, 0] = W1[5, 0] + x * deltaW6
    W1[5, 1] = W1[5, 1] + y * deltaW6
    W1[5, 2] = W1[5, 2] + z * deltaW6
    W1[5, 3] = W1[5, 3] + deltaW6
    W1[5, 4] = W1[5, 4] + deltaW6
    W1[5, 5] = W1[5, 5] + deltaW6

    W1[6, 0] = W1[6, 0] + x * deltaW7
    W1[6, 1] = W1[6, 1] + y * deltaW7
    W1[6, 2] = W1[6, 2] + z * deltaW7
    W1[6, 3] = W1[6, 3] + deltaW7
    W1[6, 4] = W1[6, 4] + deltaW7
    W1[6, 5] = W1[6, 5] + deltaW7
    if rapidMod :
        if not i%3 :
            if not noEcm :
                ecm = computeECM(W1,Ws)
                ECM.append(ecm)
    else :
        ecm = computeECM(W1,Ws)
        ECM.append(ecm)

#    print deltaW
#    print ecm
loopsDone = i
ecm = computeECM(W1,Ws)
ECM.append(ecm)
results = computeAllPrinted(W1,Ws)
print ecm
expectedResults = []
for i in range(sizeEntries):
    for j in range(sizeEntries):
        for k in range(sizeEntries):
            expectedResults.append(Sd[i][j][k])

while True :

    print("\nThe neural network has finished learning the function, what would you want to do with it ? \n 1 : See the graph of evolution of the ECM during the learning\n 2 : See the graph comparing the ideal function and the function learned\n 3 : Try to give entries to the network to see the difference between his result and the ideal result\n 4 : Quit")
    answer = raw_input("Enter your choice : ")
    if answer :
        answer =float(answer)
        if answer == 1 :
            #print ecm
            printECM(ECM)
        elif answer == 2 :
            #print graph results
            print("You asked to see the graph comparing the results of the network and the expected graph\n1 : See all the results\n2 : Results with x variations\n3 : Results with y variations\n4 : Results with z variations")
            answer2 = raw_input("Enter your choice : ")
            if answer2 :
                answer2 =float(answer2)
                if answer2 == 1 :
                    #all
                    printGraphAll(expectedResults,results)

                elif answer2 == 2 :
                    #x variations
                    y = raw_input("Enter the y value you want : ")
                    if y :
                        y =float(y)
                        y = y%(2*pi)

                        z = raw_input("Enter the z value you want : ")
                        if z :
                            z =float(z)
                            if z < 0 :
                                z = -1 * z%(1)
                            else :
                                z = z%1

                            printGraphXVariation(y,z,W1,Ws)

                elif answer2 == 3 :
                    #y variations
                    x = raw_input("Enter the x value you want : ")
                    if x :
                        x =float(x)
                        x = x%(2*pi)

                        z = raw_input("Enter the z value you want : ")
                        if z :
                            z =float(z)
                            if z < 0 :
                                z = -1 * z%(1)
                            else :
                                z = z%1

                            printGraphYVariation(x,z,W1,Ws)

                elif answer2 == 4 :
                    #z variations
                    x = raw_input("Enter the x value you want : ")
                    if x :
                        x =float(x)
                        x = x%(2*pi)

                        y = raw_input("Enter the y value you want : ")
                        if y :
                            y =float(y)
                            y = y%(2*pi)

                            printGraphZVariation(x,y,W1,Ws)

                else:
                    print "It was a false entry"
        elif answer == 3 :
            #compute a value
            print "You asked to compute a value"

            x = raw_input("Enter the x value you want : ")
            if x :
                x =float(x)
                if x > 2* pi or x < 0:
                    x = x % (2 * pi)

                y = raw_input("Enter the y value you want : ")
                if y :
                    y =float(y)
                    if y > 2* pi or y < 0:
                        y = y % (2 * pi)

                    z = raw_input("Enter the z value you want : ")
                    if z :
                        z =float(z)
                        if z < -1:
                            z = -1 * z % (1)
                        elif z>1:
                            z = ((z/2) % 1)*2

                        result = computeAll(W1,Ws,x,y,z)[0][0] * 3
                        perfectResult = sin(x) + cos(y) + z

                        print "sin("+str(x)+") + cos("+str(y)+") + "+str(z)+" : "
                        print "Result given by the network : "+str(result)
                        print "Result expected : "+str(perfectResult)

        elif answer == 4 :
            break
        else :
            print "It was a false entry, please enter a number between 1 and 4"