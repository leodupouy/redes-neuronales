#Implemente una red con aprendizaje Backpropagation que aprenda la siguiente funcion
#f(x,y,z) = (sin x + cos y + z)/3
#donde: x e y estan en el intervalo [0;2pi]
#z esta en el intervalo[-1;1]

#10 valores para cada variable

#print un graphe superpose du resultat theorique de la fonction et du resultat reel

#Toujours le meme red (combien de neurones au debut et au milieu ?)

import numpy
from numpy import *
from random import *
import matplotlib.pyplot as plt

maxLoop = 1000
#3 entradas
X = arange(0, 2 * pi+0.001, 0.2 * pi)
print "X = " + str(X)
Y = arange(0, 2 * pi+0.001, 0.2 * pi)
print "Y = " + str(Y)
Z = arange(-1, 1+0.001, 0.2)
print "Z = " + str(Z)

sizeEntries = 11
Sd = []
for x in X:
    Sd1=[]
    for y in Y:
        Sd2= []
        for z in Z:
            Sd2.append((sin(x) + cos(y) + z)/3)
        Sd1.append(Sd2)
    Sd.append(Sd1)

Beta = 0.4

ECM =[]
def computegGFirst(w, e1, e2, e3):
    h1 = w[0,0] * e1 + w[0,1] * e2 + w[0,2] * e3 + w[0,3] + w[0,4]
    h2 = w[1,0] * e1 + w[1,1] * e2 + w[1,2] * e3 + w[1,3] + w[1,4]
    h3 = w[2,0] * e1 + w[2,1] * e2 + w[2,2] * e3 + w[2,3] + w[2,4]
    h4 = w[3,0] * e1 + w[3,1] * e2 + w[3,2] * e3 + w[3,3] + w[3,4]
    h5 = w[4,0] * e1 + w[4,1] * e2 + w[4,2] * e3 + w[4,3] + w[4,4]
    g1 = tanh(Beta * h1)
    g2 = tanh(Beta * h2)
    g3 = tanh(Beta * h3)
    g4 = tanh(Beta * h4)
    g5 = tanh(Beta * h5)
    return [[g1,g2,g3,g4,g5],[h1,h2,h3,h4,h5]]

def computeGSecond(w,e1,e2,e3,e4,e5):
    h = w[0] * e1 + w[1] * e2 + w[2] * e3 + w[3] * e4 + w[5] #+ w[4] * e5
    ret = [tanh(Beta *h),h]
    return ret

def computeAll(w1,wS,e1,e2,e3):
    firstLayer = computegGFirst(w1,e1,e2,e3)
    exit = computeGSecond(wS,firstLayer[0][0],firstLayer[0][1],firstLayer[0][2],firstLayer[0][3],firstLayer[0][4])
    ret = [exit,firstLayer]
    return ret

def computeECM(w1,wS) :
    result =0
    for i in range(sizeEntries):
        for j in range(sizeEntries) :
            for k in range(sizeEntries) :
                temp = computeAll(w1, wS, X[i], Y[j], Z[k])[0][0]
                result = result + ((temp - Sd[i][j][k])**2)
    return result

def computeAllPrinted(w1,wS) :
    results = []
    for i in range(sizeEntries):
        for j in range(sizeEntries) :
            for k in range(sizeEntries) :
                result = computeAll(w1,wS, X[i], Y[j], Z[k])[0][0]
                results.append(result)
                print "sin(" + str(X[i]) + ") + cos(" + str(Y[i]) + ") + " + str(Z[i]) + " = " + str(result) +"\nresult theorical = " + str(Sd[i][j][k])
    return results


def computeDeltaS(hExit,exitExpected,exitReal):
    gPrime = Beta * (1 - tanh(hExit)**2)
    return gPrime * (exitExpected - exitReal)


W1=numpy.random.randn(5,5)
W1 = W1/2
print W1

Ws = numpy.random.randn(6)
Ws = Ws
#Ws = Ws[:,0]
print Ws

ecm = computeECM(W1,Ws)
#print "ecm = " + str(ecm)
ECM.append(ecm)
i = 0
while (ecm > 0.01 and i < maxLoop) :
#    print i
    i = i+1
    choosenX = randint(0, sizeEntries-1)
    choosenY = randint(0, sizeEntries-1)
    choosenZ = randint(0, sizeEntries-1)

    x = X[choosenX]
    y = Y[choosenY]
    z = Z[choosenZ]

    sd = Sd[choosenX][choosenY][choosenZ]

    s = computeAll(W1,Ws,x,y,z)
    exitReal = s[0][0]
    hS = s[0][1]
    g1 = s[1][0][0]
    g2 = s[1][0][1]
    g3 = s[1][0][2]
    g4 = s[1][0][3]
    g5 = s[1][0][4]
    h1 = s[1][1][0]
    h2 = s[1][1][1]
    h3 = s[1][1][2]
    h4 = s[1][1][3]
    h5 = s[1][1][4]
    deltaW = computeDeltaS(hS, sd, exitReal)
#    print "deltaW = " + str(deltaW)

    deltaW1 = Beta * (1 - (tanh(h1)) ** 2) * Ws[0]*deltaW
    deltaW2 = Beta * (1 - (tanh(h2)) ** 2) * Ws[1] * deltaW
    deltaW3 = Beta * (1 - (tanh(h3)) ** 2) * Ws[2] * deltaW
    deltaW4 = Beta * (1 - (tanh(h4)) ** 2) * Ws[3] * deltaW
    deltaW5 = Beta * (1 - (tanh(h5)) ** 2) * Ws[3] * deltaW

    ecm = computeECM(W1,Ws)
#    print "ecm =" + str(ecm) + "\n"
    Ws[0] = Ws[0] + deltaW * g1
    Ws[1] = Ws[1] + deltaW * g2
    Ws[2] = Ws[2] + deltaW * g3
    Ws[3] = Ws[3] + deltaW * g4
    Ws[4] = Ws[4] + deltaW * g5
    Ws[5] = Ws[5] + deltaW

    W1[0, 0] = W1[0, 0] + x * deltaW1
    W1[0, 1] = W1[0, 1] + y * deltaW1
    W1[0, 2] = W1[0, 2] + z * deltaW1
    W1[0, 3] = W1[0, 3] + deltaW1
    W1[0, 4] = W1[0, 4] + deltaW1

    W1[1, 0] = W1[1, 0] + x * deltaW2
    W1[1, 1] = W1[1, 1] + y * deltaW2
    W1[1, 2] = W1[1, 2] + z * deltaW2
    W1[1, 3] = W1[1, 3] + deltaW2
    W1[1, 4] = W1[1, 4] + deltaW2

    W1[2, 0] = W1[2, 0] + x * deltaW3
    W1[2, 1] = W1[2, 1] + y * deltaW3
    W1[2, 2] = W1[2, 2] + z * deltaW3
    W1[2, 3] = W1[2, 3] + deltaW3
    W1[2, 4] = W1[2, 4] + deltaW3

    W1[3, 0] = W1[3, 0] + x * deltaW4
    W1[3, 1] = W1[3, 1] + y * deltaW4
    W1[3, 2] = W1[3, 2] + z * deltaW4
    W1[3, 3] = W1[3, 3] + deltaW4
    W1[3, 4] = W1[3, 4] + deltaW4

    W1[4, 0] = W1[4, 0] + x * deltaW5
    W1[4, 1] = W1[4, 1] + y * deltaW5
    W1[4, 2] = W1[4, 2] + z * deltaW5
    W1[4, 3] = W1[4, 3] + deltaW5
    W1[4, 4] = W1[4, 4] + deltaW5

    ECM.append(ecm)

#    print deltaW
#    print ecm

results = computeAllPrinted(W1,Ws)
plt.title('evolution of ECM')
plt.plot(ECM)
plt.ylabel('ECM')
plt.show()
expectedResults = []
for i in range(sizeEntries):
    for j in range(sizeEntries):
        for k in range(sizeEntries):
            expectedResults.append(Sd[i][j][k])

plt.title('Comparison of the expected results and the real results')
plt.plot(expectedResults, 'r', results, 'b')
plt.show()

