#Implemente un perceptron simple que aprenda las funciones logicas XOR de 4 entradas

import numpy
import time
from numpy import *
from random import *
import matplotlib.pyplot as plt
printAll = False
#4 entradas
E1 = [ 1, 1, 1, 1,-1,-1,-1,-1, 1, 1, 1,-1,-1,-1, 1,-1]
E2 = [ 1, 1, 1,-1, 1, 1, 1,-1,-1,-1, 1,-1,-1, 1,-1,-1]
E3 = [ 1, 1,-1, 1, 1, 1,-1, 1,-1, 1,-1,-1, 1,-1,-1,-1]
E4 = [ 1,-1, 1, 1, 1,-1, 1, 1, 1,-1,-1, 1,-1,-1,-1,-1]

Sd = [-1, 1, 1, 1, 1, 1,-1,-1,-1,-1,-1, 1, 1, 1, 1,-1]


ECM =[]
def computegGFirst(w, e1, e2, e3, e4):
    h1 = w[0,0] * e1 + w[0,1] * e2 + w[0,2] * e3 + w[0,3] * e4 + w[0,4]
    h2 = w[1,0] * e1 + w[1,1] * e2 + w[1,2] * e3 + w[1,3] * e4 + w[1,4]
    h3 = w[2,0] * e1 + w[2,1] * e2 + w[2,2] * e3 + w[2,3] * e4 + w[2,4]
    h4 = w[3,0] * e1 + w[3,1] * e2 + w[3,2] * e3 + w[3,3] * e4 + w[3,4]
    h5 = w[4,0] * e1 + w[4,1] * e2 + w[4,2] * e3 + w[4,3] * e4 + w[4,4]
    g1 = tanh(Beta * h1)
    g2 = tanh(Beta * h2)
    g3 = tanh(Beta * h3)
    g4 = tanh(Beta * h4)
    g5 = tanh(Beta * h5)
    return [[g1,g2,g3,g4,g5],[h1,h2,h3,h4,h5]]

def computeGSecond(w,e1,e2,e3,e4,e5):
    h = w[0] * e1 + w[1] * e2 + w[2] * e3 + w[3] * e4 + w[5] + w[4] * e5
    ret = [tanh(Beta *h),h]
    return ret

def computeAll(w1,wS,e1,e2,e3,e4):
    firstLayer = computegGFirst(w1,e1,e2,e3,e4)
    exit = computeGSecond(wS,firstLayer[0][0],firstLayer[0][1],firstLayer[0][2],firstLayer[0][3],firstLayer[0][4])
    ret = [exit,firstLayer]
    return ret

def computeECM(w1,wS) :
    result =0
    for i in range(16):
        temp = computeAll(w1, wS, E1[i], E2[i], E3[i], E4[i])[0][0]
        result = result + ((temp - Sd[i])**2)
    return result

def computeAllPrinted(w1,wS) :
    result =0
    sumError = 0
    for i in range(16):
        result = computeAll(w1,wS, E1[i], E2[i], E3[i], E4[i])[0][0]
        if printAll :
            print str(E1[i]) + " XOR " + str(E2[i]) + " XOR " + str(E3[i]) + " XOR " + str(E4[i]) + " = " + str(result) +"\nresult theorical = " + str(Sd[i])
        if sign(result) != sign(Sd[i]) :
            sumError = sumError + 1
    if printAll :
        print "Sign errors : " + str(sumError)
    return sumError


def computeDeltaS(hExit,exitExpected,exitReal):
    gPrime = Eta * Beta * (1 - tanh(hExit)**2)
    return gPrime * (exitExpected - exitReal)

def printECM(ECM) :
    plt.title('evolution of ECM for XOR 4 entries\n5 Neurons\nBeta : ' + str(Beta) + ' Eta : ' + str(Eta) + ' Loops : ' + str(loops))
    plt.plot(ECM)
    plt.ylabel('ECM')
    plt.show()


def mainLoop(maxLoop) :
    minEcm = 1000
    W1=numpy.random.randn(5,5)
    W1 = W1/2
    if printAll:
        print W1

    Ws = numpy.random.randn(6)
    Ws = Ws
    if printAll:
        print Ws

    En=[0,0,0,0]
    ECM = []
    ecm = computeECM(W1,Ws)
    ECM.append(ecm)
    i = 0.0
    lastpercent = 1
    start_time = time.time()
    intermediate_time = start_time
    while (ecm > 0.01 and i < maxLoop) :
        i = i+1
        if ((i / maxLoop) > (lastpercent / 100.0)):
            elapsed_time = time.time() - intermediate_time

            intermediate_time = time.time()
            print "estimated time left : " + str(elapsed_time * (100 - lastpercent)).split(".")[0] + "seconds"
            print str(lastpercent) + "%"
            lastpercent = lastpercent + 1

        Choosen = randint(0, 15)
        En[0] = E1[Choosen]
        En[1] = E2[Choosen]
        En[2] = E3[Choosen]
        En[3] = E4[Choosen]

        sd = Sd[Choosen]

        s = computeAll(W1,Ws,En[0],En[1],En[2],En[3])
        exitReal = s[0][0]
        hS = s[0][1]
        g1 = s[1][0][0]
        g2 = s[1][0][1]
        g3 = s[1][0][2]
        g4 = s[1][0][3]
        g5 = s[1][0][4]

        h1 = s[1][1][0]
        h2 = s[1][1][1]
        h3 = s[1][1][2]
        h4 = s[1][1][3]
        h5 = s[1][1][4]

        deltaW = computeDeltaS(hS, sd, exitReal)

        deltaW1 = Eta * Beta * (1 - (tanh(h1)) ** 2) * Ws[0] * deltaW
        deltaW2 = Eta * Beta * (1 - (tanh(h2)) ** 2) * Ws[1] * deltaW
        deltaW3 = Eta * Beta * (1 - (tanh(h3)) ** 2) * Ws[2] * deltaW
        deltaW4 = Eta * Beta * (1 - (tanh(h4)) ** 2) * Ws[3] * deltaW
        deltaW5 = Eta * Beta * (1 - (tanh(h5)) ** 2) * Ws[4] * deltaW

        Ws[0] = Ws[0] + deltaW*g1
        Ws[1] = Ws[1] + deltaW*g2
        Ws[2] = Ws[2] + deltaW*g3
        Ws[3] = Ws[3] + deltaW*g4
        Ws[4] = Ws[4] + deltaW*g5
        Ws[5] = Ws[5] + deltaW

        W1[0, 0] = W1[0, 0] + En[0] * deltaW1
        W1[0, 1] = W1[0, 1] + En[1] * deltaW1
        W1[0, 2] = W1[0, 2] + En[2] * deltaW1
        W1[0, 3] = W1[0, 3] + En[3] * deltaW1
        W1[0, 4] = W1[0, 4] + deltaW1

        W1[1, 0] = W1[1, 0] + En[0] * deltaW2
        W1[1, 1] = W1[1, 1] + En[1] * deltaW2
        W1[1, 2] = W1[1, 2] + En[2] * deltaW2
        W1[1, 3] = W1[1, 3] + En[3] * deltaW2
        W1[1, 4] = W1[1, 4] + deltaW2

        W1[2, 0] = W1[2, 0] + En[0] * deltaW3
        W1[2, 1] = W1[2, 1] + En[1] * deltaW3
        W1[2, 2] = W1[2, 2] + En[2] * deltaW3
        W1[2, 3] = W1[2, 3] + En[3] * deltaW3
        W1[2, 4] = W1[2, 4] + deltaW3

        W1[3, 0] = W1[3, 0] + En[0] * deltaW4
        W1[3, 1] = W1[3, 1] + En[1] * deltaW4
        W1[3, 2] = W1[3, 2] + En[2] * deltaW4
        W1[3, 3] = W1[3, 3] + En[3] * deltaW4
        W1[3, 4] = W1[3, 4] + deltaW4

        W1[4, 0] = W1[4, 0] + En[0] * deltaW5
        W1[4, 1] = W1[4, 1] + En[1] * deltaW5
        W1[4, 2] = W1[4, 2] + En[2] * deltaW5
        W1[4, 3] = W1[4, 3] + En[3] * deltaW5
        W1[4, 4] = W1[4, 4] + deltaW5

        ecm = computeECM(W1, Ws)
        if ecm < minEcm :
            minEcm = ecm
        ECM.append(ecm)

    errors = computeAllPrinted(W1,Ws)
    if errors != 0 :
        return [False,errors]
    else :
        return [True,[W1,Ws], ECM]

def check(string) :
    if string :
        value = int(string)
        if value == 1 or value == -1 :
            return True
        else :
            return False
    return False


loops = 100000
printAll = False
Beta = 0.7
Eta = 0.75

results = mainLoop(loops)

if not results[0] :
    print("The network failed to learn well the function, there is still "+str(results[1])+" entries combinations with a sign problem")
else :
    while True :

        print("\nThe neural network has finished learning the function, what would you want to do with it ? \n 1 : See the graph of evolution of the ECM during the learning\n 2 : See the comparison between all the results given by the network and the ideal results\n 3 : Try to give entries to the network to see the difference between his result and the ideal result\n 4 : Quit")
        answer = raw_input("Enter your choice : ")
        if answer :
            answer = int(answer)
            if answer == 1 :
                #print ecm
                printECM(results[2])
            elif answer == 2 :
                #print all results
                print("You asked to see the comparison between all the results given by the network and the ideal results")
                printAll = True
                computeAllPrinted(results[1][0],results[1][1])
                raw_input("Press enter to continue")
            elif answer == 3 :
                #compute a value
                print "You asked to compute a value, enter values wich are 1 or -1"

                e1 = raw_input("Enter the first value you want : ")
                if check(e1) :
                    e1 = int(e1)
                else:
                    print("It was invalid, we put 1 instead")
                    e1=1
                e2 = raw_input("Enter the second value you want : ")
                if check(e2):
                    e2 = int(e2)
                else:
                    print("It was invalid, we put 1 instead")
                    e2=1
                e3 = raw_input("Enter the third value you want : ")
                if check(e3):
                    e3 = int(e3)
                else:
                    print("It was invalid, we put 1 instead")
                    e3=1
                e4 = raw_input("Enter the fourht value you want : ")
                if check(e4):
                    e4 = int(e4)
                else:
                    print("It was invalid, we put 1 instead")
                    e4=1
                result = computeAll(results[1][0],results[1][1],e1,e2,e3,e4)[0][0]
                perfectResult = e1+e2+e3+e4
                if perfectResult == 2 or perfectResult == -2 :
                    perfectResult = 1
                else :
                    perfectResult = -1
                print str(e1)+" XOR "+str(e2)+" XOR "+str(e3)+" XOR "+str(e4)
                print "Result given by the network : "+str(result)
                print "Result expected : "+str(perfectResult)

            elif answer == 4 :
                break
            else :
                print "It was a false entry, please enter a number between 1 and 4"